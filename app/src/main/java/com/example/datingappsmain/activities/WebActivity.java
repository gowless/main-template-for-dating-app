package com.example.datingappsmain.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.datingappsmain.GlobalClass;
import com.example.datingappsmain.R;
import com.example.datingappsmain.utils.SharedPrefsClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import static com.example.datingappsmain.GlobalClass.finalInputLine;
import static com.example.datingappsmain.GlobalClass.inputLine;
import static com.example.datingappsmain.GlobalClass.remoteResponse;
import static com.example.datingappsmain.GlobalClass.sharedPrefsClass;

public class WebActivity extends AppCompatActivity {
    //объявляем веб вью
    private WebView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        //***БЛОК ВЫПОЛНЕНИЯ ОСНОВНЫХ МЕТОДОВ ПО РАБОТЕ С WEBVIEW***//

        //1)
        // init content
        initContent();


        //2)
        //выключаем ограничение по треду
        offStrictPolicy();


        //3)
        //читаем текст (ссылку) с изначальной web-page и присваиваем глобальной переменной
        try {
            getMainUrl(remoteResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //4)
        //включаем здесь всё необходимое для веб вью
        turnWebViewSettings();


        //5)
        //слушаем коллбеки с загруженных страниц
        setCallbacks();



        //6)
        //проверяем на первый запуск и прокидывем ссылку в веб вью
        checkOnFirstLaunch();



    }

    //инициализируем всё нужное
    private void initContent(){
        //webview элемент
        view = findViewById(R.id.webview);
        //префы для сохранения ссылки и состояния приложения
        sharedPrefsClass  = new SharedPrefsClass(this);

    }

    //выключаем ограничение по основному треду
    private void offStrictPolicy(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }


    //делаем коллбеки по загрузке страниц
    private void setCallbacks(){
        view.setWebViewClient(new WebViewClient() {
            public void onPageFinished(@Nullable WebView view, @Nullable String url) {
                //засовываем ссылку последней сохранённой страницы в префы
                sharedPrefsClass.setPrefsUrl(url);
            }

            public void onReceivedError(@Nullable WebView view, @Nullable WebResourceRequest request, @Nullable WebResourceError error) {
                super.onReceivedError(view, request, error);
                //обрабатываем ошибки
            }
        });
    }



    //получаем ссылку путём считки веб страницы, ссылку на которую мы получили из ремоут конфига и присваиваем глобальной переменной
    private void getMainUrl(String firstUrl) throws IOException {
        try  {
            Log.d("url1", "test2");
            URL oracle = new URL(firstUrl);
            BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
            while ((inputLine = in.readLine()) != null)
                finalInputLine = inputLine;
                Log.d("url1", inputLine);
            in.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //включаем необходимые функции
    public void turnWebViewSettings(){
        view.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        view.getSettings().setAppCacheEnabled(true);
        view.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        view.getSettings().setGeolocationEnabled(true);
        view.getSettings().setUseWideViewPort(true);
        view.getSettings().setLoadWithOverviewMode(true);
        view.getSettings().setAllowContentAccess(true);
        view.getSettings().setDatabaseEnabled(true);
        view.getSettings().setLoadsImagesAutomatically(true);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        view.getSettings().setDomStorageEnabled(true);
        view.getSettings().setAppCacheEnabled(true);
        view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        view.getSettings().setBuiltInZoomControls(true);
        view.getSettings().setSupportZoom(true);
        view.setWebChromeClient(new WebChromeClient());
        view.getSettings().setJavaScriptEnabled(true);
    }

    //чекаем на первый запуск
    private void checkOnFirstLaunch(){
        //проверяем на первый запуск
        //если не первый ->
        if (sharedPrefsClass.getFirstLaunch()){
            //загружаем последнюю ссылку, вытаскиваем её из префов
            view.loadUrl(sharedPrefsClass.getPrefsUrl());
        }
        //если первый ->
        else {
                //загружаем полученную из начального web-pag'a cсылку
                view.loadUrl(getTargetUrl());
            //ставим флаг на первый запуск
            sharedPrefsClass.setFirstLaunch();
        }
    }

    //добавляем sub6 (имя пакета для дейтинг прилок) и получаем на выходе конечную ссылку
    private String getTargetUrl(){
       return finalInputLine = finalInputLine + "?sub6="+getPackageName();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (view.canGoBack()) {
                        view.goBack();
                    } else {
                        this.finishAffinity();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}