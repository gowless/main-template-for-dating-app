package com.example.datingappsmain.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsClass {

    private static SharedPreferences settings;
    private static SharedPreferences.Editor editor;

    public SharedPrefsClass(Context context) {
        settings = context.getSharedPreferences("LOCAL", 0);
        editor = settings.edit();
    }

    //получаем последнюю ссылку
    public String getPrefsUrl() {
        return settings.getString("url", "");
    }

    //сохраняем последнюю ссылку
    public void setPrefsUrl(String string) {
        editor.putString("url", string);
        editor.apply();
    }

    //ставим значение первого запуска в WebActivity
    public void setFirstLaunch() {
        editor.putBoolean("first", true);
        editor.apply();
    }

    //получаем значение первого запуска в WebActivity
    public Boolean getFirstLaunch() {
        return settings.getBoolean("first", false);
    }

    //получаем значение в проверку в SplashScreen
    public  Boolean getConfigNotPassed(){
        return settings.getBoolean("configPass", false);
    }

    //если не прошёл проверку, то ставим в SplashScreen = true
    public  void setConfigNotPassed(Boolean bool){
        editor.putBoolean("configPass", bool);
        editor.apply();
    }


}
